
use std::env;
use url::Url;

pub struct Config {
    pub service_url: Url,
    pub username: String,
    pub password: String
}

pub fn parse_config() -> Result<Config, &'static str> {
    println!("Grabbing arguments");
    // TODO Stop using unwrap() so much, John.
    let service = env::var("BSKY_SERVICE").unwrap();
    let service_url = Url::parse(&service).unwrap();
    let username = env::var("BSKY_USERNAME").unwrap();
    let password = env::var("BSKY_PASSWORD").unwrap();

    Ok(Config {
        service_url,
        username,
        password
    })
}